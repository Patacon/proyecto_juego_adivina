/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appinout;

import java.awt.Robot;

/**
 *
 * @author Mica y Tomy
 */
public class Vista {
    private String screen[];
    private String titulo = "Adivina si puedes";
    private String instrucciones = "Tienes que ingresar un número dentro de los valores posibles y adivinar";
    private String viditas = "3";
    private String record = "3";
    private String valoresPosibles = "1:20%, 2:25%, 3:20%";
    private String pistas = "";
    private String ingresarValor = "Ingresar Valor: ";
    private Datos param;
   
    // constructor por defecto    
     Vista(){
        screen = new String[7];
    }
    
     // constructor por parametros
    Vista(Datos datos){
        screen = new String[7];
        param = datos;
    }
    
    // borrar toda la pantalla
    public void borrarPantalla() {
        try {
            java.awt.Robot pressbot = new java.awt.Robot();
            pressbot.keyPress(17); // Holds CTRL key.
            pressbot.keyPress(76); // Holds L key.
            pressbot.keyRelease(17); // Releases CTRL key.
            pressbot.keyRelease(76); // Releases L key.
            java.util.concurrent.TimeUnit.MILLISECONDS.sleep(100);
        } 
        catch (Exception e){
            System.out.println(e);
        }
    }
    
    //Comenzamos a crear métodos

    //pantallaInicio servirá para mostrar lo que tiene que mostrar
    public void pantallaInicio() {
        screen[0] = titulo;
        screen[1] = instrucciones;
    }
    //vidasRestantes servirá para disminuir las vidas
    public void vidasRestantes() {
        screen[2] = Integer.toString(param.intentos);
    }
    
    public void suRecord() {
        screen[3] = record;
    }
    
    public void valoresPos() {
        screen[4] = valoresPosibles;
    }
    
    public void pistas() {
        screen[5] = pistas;
    }
            
    public void valor() {
        screen[6] = ingresarValor;         
   }
   
   public void ganador() {
       pistas = ("¡Ganaste guachin!");
       ingresarValor = ("");
       refrescarPantalla();
   }
   public void perdedor() {
       pistas = ("Fin del Juego. Perdiste");
       ingresarValor = ("");
       refrescarPantalla();
   }
   public void mayor() {
       pistas = ("Erraste. El número es mayor");
       refrescarPantalla();
   }
   public void menor() {
       pistas = ("Erraste. El número es menor");
       refrescarPantalla();
   }
    public void refrescarPantalla() {
        for (int i = 0 ; i < screen.length ; i++){
            pantallaInicio();
            vidasRestantes();
            suRecord();
            valoresPos();
            pistas();
            valor();
            System.out.println(screen[i]);
        }
        

        
}    
}
